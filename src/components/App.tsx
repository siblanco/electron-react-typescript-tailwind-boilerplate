import React from 'react';

export const App = () => (
	<div className="p-6">
		<img
			className="w-32"
			src={require('../assets/images/logo.png')}
			alt="Siblanco Production"
		/>
		<h1 className="mt-4 text-3xl text-teal-500 font-bold">Nice one parcel</h1>
	</div>
);
